import React from 'react';
import { graphql } from 'gatsby';

export const query = graphql`
  query HomeTemplateQuery {
    markdownRemark(frontmatter: { template: { eq: "home" } }) {
      frontmatter {
        title
        type
        template
      }
    }
  }
`;

const HomePage = ({ data }) => {
  const { frontmatter: page } = data.markdownRemark;

  return (
    <div>
      <h1 className="text-6xl">{page.title}</h1>
    </div>
  );
};

export default HomePage;
