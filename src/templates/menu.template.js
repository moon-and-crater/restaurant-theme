import React from 'react';
import { graphql } from 'gatsby';

export const query = graphql`
  query MenuTemplateQuery {
    markdownRemark(frontmatter: { template: { eq: "menu" } }) {
      frontmatter {
        title
        type
        template
      }
    }
  }
`;

const MenuPage = ({ data }) => {
  const { frontmatter: page } = data.markdownRemark;

  return (
    <div>
      <h1>{page.title}</h1>
      <p>This is the {page.template} template.</p>
    </div>
  );
};

export default MenuPage;
