exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions;

  const query = await graphql(`
    query {
      allMarkdownRemark {
        edges {
          node {
            frontmatter {
              title
              type
              template
            }
          }
        }
      }
    }
  `);

  const { edges: pages } = query.data.allMarkdownRemark;

  pages.forEach(({ node }) => {
    if (node.frontmatter.type === 'page') {
      const { title, template } = node.frontmatter;
      const url = template !== 'home' ? createUrl(title) : '';

      createPage({
        path: `/${url}`,
        component: `${__dirname}/src/templates/${template}.template.js`,
      });
    }
  });
};

const createUrl = title => title.toLowerCase().replace(' ', '-');
